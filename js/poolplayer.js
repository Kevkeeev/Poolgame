class PoolPlayer {
	
	constructor (name, /* boolean */ stripedBalls ) {
		this.name = name;
		this.score = 0;
		this.turn = 0;
		this.pocketed = [];
		
		this.toPocket = [];
		for(var i=1; i<8; i++) {
			this.toPocket.push( (stripedBalls ? i+8 : i) );
		}
		debug("[INIT] Created Player: "+ this.name +", toPocket: "+ JSON.stringify( this.toPocket ), 1 );
	}
	
	pocketBall (number) {
		this.pocketed.push(number);

		if( this.toPocket.indexOf( number ) >= 0 ){
			this.score++;
			this.toPocket.splice( this.toPocket.indexOf( number ), 1); //remove 1 element at <number>'s index.
			
		} else if( number == 8) {
			if( toPocket.length == 0) {
				debug(this.name +" (Player "+ this.number +") has won!", 1);
				alert(this.name +" (Player "+ this.number +") has won!");
			} else {
				debug(this.name +" (Player "+ this.number +") pocketed the 8-ball, and has lost!", 1);
				alert(this.name +" (Player "+ this.number +") pocketed the 8-ball, and has lost!");
			}
		} else {
			//player pocketed a ball, that he shouldn't have, but it's not the 8-ball.
			
			alert("Boo, "+ players[curPlayer].name +", that's the wrong ball!");
			//make the player feel bad.
			
		}
		
	}
	
}
