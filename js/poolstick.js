class PoolStick {
	
	constructor () {
		var stickLength = 148.6;
		var tipRadius = 0.7;
		var ballRadius = 2.86;

		//Move center of the cue model, to the edge of the tip + half a ball further.
		//official size: 148.6 cm long, 14mm tip diameter
		var stickGeo = new THREE.CylinderGeometry(tipRadius, 1.5, stickLength, 32, stickLength);
		stickGeo.translate(0, -( (stickLength/2) + tipRadius + ballRadius ) , 0);
		var stickMat = new THREE.MeshLambertMaterial( {color : 0x8b5a2b }); // brown
		var stick = new THREE.Mesh( stickGeo, stickMat );
	
		//Round tip: 14mm diameter
		var tipGeo = new THREE.SphereGeometry(tipRadius, 32, 32);
		var tipMat = new THREE.MeshBasicMaterial( {color: 0xadd8e6 }); // light blue
		var tip = new THREE.Mesh( tipGeo, tipMat );
		tip.position.y = - (tipRadius + ballRadius);
		
		var stickBSP = new ThreeBSP( stick );
		var tipBSP = new ThreeBSP( tip );
		
		var resultBSP = stickBSP.union(tipBSP)//;.toMesh( stickMat );
		//result.geometry.computeVertexNormals();
		
		return resultBSP;
	}
	
}
	
	function setColourAndReturn(resultBSP, newColour){
		var material = new THREE.MeshLambertMaterial( {color: newColour });
		var stick = resultBSP.toMesh( material );
		stick.geometry.computeVertexNormals();
		stick.rotateZ(- (110 / 90) * Math.PI / 2); //raise stick slightly above table
		return stick;
	}

